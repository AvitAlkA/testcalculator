package SDK;

/**
 * Represent a single event in test report
 * 
 * @author avidan
 *
 */
public class reportEvent {

	private String title;
	private reportEventStatusTypes status;
	private String note;

	// constructor
	reportEvent(String argTitle, reportEventStatusTypes argStatus, String argNote) {

		this.title = argTitle;
		this.status = argStatus;
		this.note = argNote;
	}

	public void print() {
		System.out.println(title + "\t |" + status + "\t |" + note);

	}
}
