package SDK;

import java.util.ArrayList;

public class reporter {
	// collection list of report events
	private ArrayList<reportEvent> events = new ArrayList<reportEvent>();

	private void InsertEvent(String argTitle, reportEventStatusTypes argStatus, String argNote) {

		reportEvent re = new reportEvent(argTitle, argStatus, argNote);

		events.add(re);
	}

	public void Pass(String argTitle, String argNote) {

		InsertEvent(argTitle, reportEventStatusTypes.PASS, argNote);

	}

	public void Failed(String argTitle, String argNote) {

		InsertEvent(argTitle, reportEventStatusTypes.FAILED, argNote);

	}

	public void Error(String argTitle, String argNote) {

		InsertEvent(argTitle, reportEventStatusTypes.ERROR, argNote);

	}

	public void Info(String argTitle, String argNote) {

		InsertEvent(argTitle, reportEventStatusTypes.INFO, argNote);

	}

	public void print() {
		for (reportEvent re : events) {
			re.print();
		}
	}
}
