
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DatePage {
    private final static String PAGE_URL = "https://www.calculator.net/date-calculator.html";
    private WebDriver driver;

    // constructor
    public DatePage(WebDriver d) {
        this.driver = d;
    }

    public void openPage() {
        driver.navigate().to(PAGE_URL);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // driver.manage().window().maximize();
        // report.Info("PercentagePage", "Open " + PAGE_URL);
    }

    /**
     * Locators
     */
    public Select mounth1() {
        Select dropDown = new Select(driver.findElement(By.id("today_Month_ID")));
        return dropDown;
    }

    public Select day1() {
        Select dropDown = new Select(driver.findElement(By.id("today_Day_ID")));
        return dropDown;
    }

    public WebElement year1() {
        return driver.findElement(By.id("today_Year_ID"));
    }

    public Select mounth2() {
        Select dropDown = new Select(driver.findElement(By.id("ageat_Month_ID")));
        return dropDown;
    }

    public Select day2() {
        Select dropDown = new Select(driver.findElement(By.id("ageat_Day_ID")));
        return dropDown;
    }

    public WebElement year2() {
        return driver.findElement(By.id("ageat_Year_ID"));
    }

    public WebElement getCalcButton() {
        return driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/table[2]/tbody/tr/td[1]/input"));
    }

    public WebElement getResult() {
        return driver.findElement(By.xpath("/html/body/div[3]/div[1]/div[2]"));

    }

    public void clearYear(WebElement element) {
        Actions act = new Actions(driver);
        act.doubleClick(element).build().perform();
    }

    public void printExpected(String getExpected) {
        System.out.println(getExpected);
    }

}
